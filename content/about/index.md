+++
title= "About Me"
+++

{{<image-center src="pp-3.jpg" alt="sketch of me" size="40%" >}}

## BURAK TÜRKÖZ

Hi! I am a Helsinki-based industrial designer and maker, specializing in rapid prototyping. 

After completing my industrial design education at Middle East Technical University, Turkey, I decided to pursue an interdisciplinary master’s degree to broaden my outlook on product design. I started a master's in New Media at Aalto University, with a minor in Digital Fabrication. During my studies, I became interested in maker culture and worked as a workshop assistant at makerspaces, including [Aalto Fab Lab](https://studios.aalto.fi/fablab/). I explored ways of using rapid prototyping tools and electronics creatively. For my [master’s thesis](https://burakturkoz.gitlab.io/design-portfolio/mobile-lab/), I designed and built a mobile makerspace for children’s digital fabrication education. In 2024, I completed the MIT-affiliated prototyping course, [Fab Academy](https://fabacademy.org/2024/labs/aalto/students/burak-turkoz/). 

My approach to design is user-centric and playful. Be it product design, exhibition design, creative coding or physical computing, I strive to explore distinct user experiences and play elements. Working in the intersection of the digital and the physical, I am always passionate about hands-on experimentation. I combine my design practice with other topics like [history](https://burakturkoz.gitlab.io/design-portfolio/sands-of-time), theater, and [folklore](https://burakturkoz.gitlab.io/design-portfolio/toz). 

I am open to freelance work in product design, prototyping or maker education. I can offer industrial design services from the concept development stage to the high-fidelity prototyping stage. You can reach out to me at <a href="mailto:burak_turkoz@outlook.com">burak_turkoz@outlook.com</a> or contact me through <a href = "https://www.linkedin.com/in/burak-turkoz/" target="_blank">LinkedIn.</a>

<a href="/design-portfolio/docs/burak-turkoz-cv.pdf" download="burak-turkoz-cv.pdf" 
   style="display: block; 
          width: fit-content; 
          margin: 20px auto; 
          padding: 20px; 
          text-align: center; 
          text-decoration: none; 
          color: #000; 
          border: 3px solid #353535; 
          border-radius: 5px; 
          box-shadow: 0 2px 2px rgba(0, 0, 0, 0.3); 
          font-family: 'Golos Text', sans-serif; 
          font-weight: 500; 
          transition: background-color 0.3s ease, color 0.3s ease;"
   onmouseover="this.style.backgroundColor='rgb(241, 129, 44)'; this.style.color='#fff';" 
   onmouseout="this.style.backgroundColor=''; this.style.color='#000';">
  Download CV
</a>