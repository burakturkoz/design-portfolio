+++
title="Visual Communication Design Portfolio"
+++

{{<image src="indexlist.png" alt="title" size="100%" >}}

<h2 style="color: rgb(101, 12, 229); font-size: 70px; margin-bottom: 10px ">1</h2>

{{<image src="g1.jpg" alt="title" size="100%" >}}

"Homo Ludens" is a series of playing cards, centered around psychodramatic practices. It has three sets: "family", "couples", and "group", each aimed towards a different player base. The players take turns to ask each other the questions on the cards, learning more of each other and forming deeper connections in the process.

I did the logo design for the brand and the graphic design for the playing cards. 

**Project Type:** Individual work, Freelance

**Year:** 2022-2023

<h3 style="color: rgb(101, 12, 229); margin-bottom: 10px ">Logo Design</h3>

Homo Ludens (Latin for "playing human") is a model developed by Johan Huizinga. It discusses the importance of play elements to develop cultural abilities. Similar to "Homo erectus" or "Homo sapiens", the term forms a connection to the central traits of humans. I based the logo design these connotations of the word. 

{{<image src="g2.jpg" alt="title" size="100%" >}}

<h3 style="color: rgb(101, 12, 229); margin-bottom: 10px ">Card Design</h3>

Homo Ludens has three sets: "couples", "group" and "family". I designed a logo relating to the central concepts of each set. The game itself was given a name of it's own along the design process: "İçgörü", meaning insight in Turkish.

{{<image src="g3.jpg" alt="title" size="100%" >}}

{{<image src="g4.jpg" alt="title" size="100%" >}}

{{<image src="g5.jpg" alt="title" size="100%" >}}

<h3 style="color: rgb(101, 12, 229); margin-bottom: 10px ">Prints</h3>

Six sets in total went into production, with each set having two volumes. Besides the individual cards, I designed their boxes as well.

{{<image-center src="g6.JPG" alt="prints" size="100%" >}}
{{<image-center src="g7.JPG" alt="prints" size="100%" >}}
{{<image-center src="g9.JPG" alt="prints" size="100%" >}}
{{<image-center src="g8.JPG" alt="prints" size="100%" >}}
{{<image-center src="g10.JPG" alt="prints" size="100%" >}}
{{<image-center src="g11.JPG" alt="prints" size="100%" >}}

<h2 style="color: rgb(20, 168, 55); font-size: 70px; margin-bottom: 10px ">2</h2>

{{<image src="p1.jpg" alt="title" size="100%" >}}

This project is a visual identity and portfolio design for myself. It started with re-designing my PDF portfolio to turn it into a website. I combined my old logo featuring a toucan with my comic artsyle to form a new visual identity. 

I made the website on Gitlab using HTML, Markdown and Hugo. Coding it myself also provided guidelines for the final design.

**Project Type:** Individual work

**Year:** 2023

{{<image src="p2.jpg" alt="portfolio" size="100%" >}}

<h3 style="color: rgb(20, 168, 55); margin-bottom: 10px ">Typefaces</h3>

{{<image src="p4.jpg" alt="portfolio" size="100%" >}}

<h3 style="color: rgb(20, 168, 55); margin-bottom: 10px ">The Toucans</h3>

I made unique toucan illustrations for every project in my portfolio. Each one has a theme related to the content of the project. They are mainly featured in the navigation menu boxes. The color palette of the toucan also determines the color of the titles in the project page. 

{{<image src="p3.jpg" alt="portfolio" size="100%" >}}

<h3 style="color: rgb(20, 168, 55); margin-bottom: 10px ">Navigation Menu Boxes</h3>

The homepage of my portfolio website has boxes for navigating to the project pages. Each box introduces the project briefly and gives a preview when hovered over. 

{{< box link="https://fabacademy.org/2024/labs/aalto/students/burak-turkoz/" no="box-df" title="FAB ACADEMY" subtitle="Global Course on Rapid Prototyping" image="toucan-fab.png" hoverImage="df-hover.jpg" alt="fab-toucan" >}}
Documentation Page, Digital Fabrication / 2023-24
{{< /box >}}

{{< box link="psp-v" no="box-psp" title="PSP: V" subtitle="Next-Generation PlayStation Portable" image="toucan-psp.png" hoverImage="psp-hover.jpg" alt="psp-toucan" >}}
Industrial Design / 2021
{{< /box >}}

<h2 style="color: rgb(230, 83, 37); font-size: 70px; margin-bottom: 10px ">3</h2>

{{<image src="t1.jpg" alt="title" size="100%" >}}

Tunelands is a music education website for primary school students and teachers, to be used for distance learning. The design is based on user research through interviews with primary school students and teachers. Pedagocial principles outlined during research impacted our design decisions. 

**Project Type:** Group work

**Group Members:** Berna Ertenli, Burak Türköz, Ezgi Çakmak, Leyla Engin

**Year:** 2021

{{<image src="t2.jpg" alt="visual id" size="100%" >}}

<h3 style="color: rgb(230, 83, 37); margin-bottom: 10px ">The Four Musical Islands</h3>

Tunelands uses gamification to teach students about musical concepts. The games are divided into four islands, with unique learning outcomes. 

{{<image src="t3.jpg" alt="islands" size="100%" >}}

<h3 style="color: rgb(230, 83, 37); margin-bottom: 10px ">Student Interface</h3>

Students play different minigames in the four island. They are rewarded at the end of each game, and they can use the rewards customize the islands.

{{<image-center src="t8.jpg" alt="main interface" size="100%" >}}

{{<image-center src="t9.jpg" alt="login" size="100%" >}}

{{<image-center src="t5.jpg" alt="game" size="100%" >}}

{{<image-center src="t4.jpg" alt="game" size="100%" >}}

<h3 style="color: rgb(230, 83, 37); margin-bottom: 10px ">Teacher Interface</h3>

Virtual classes can also be held through Tunelands. The teacher can give communal tasks during the class for the students to do together. We designed a separate user interface for the teachers to plan and manage their classes.

{{<image-center src="t10.jpg" alt="game" size="100%" >}}

{{<image-center src="t12.jpg" alt="game" size="100%" >}}

<h2 style="color: rgb(5, 5, 147); font-size: 70px; margin-bottom: 10px ">4</h2>

{{<image-center src="s1.jpg" alt="title" size="100%" >}}

The Rosetta Stone is an artifact that played a pivotal role in uncovering the secrets of the Ancient Egyptian hieroglyphs. Today, it is exhibited in the British Museum. This design is an alternative exhibition space for the artifact, highlighting the linguistic narrative surrounding it.

**Project Type:** Individual work

**Year:** 2022

<h3 style="color: rgb(5, 5, 147); margin-bottom: 10px ">Rosetta Stone</h3>

{{<image src="sands-rosetta.jpg" alt="rosetta stone research" size="100%" >}}

<h3 style="color: rgb(5, 5, 147); margin-bottom: 10px ">How To Create Spatial Experiences</h3>

{{<image src="sands-research.jpg" alt="experience research" size="100%" >}}

<h3 style="color: rgb(5, 5, 147); margin-bottom: 10px ">Final Renders</h3>

{{<image src="sands-f-1.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-2.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-3.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-4.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-5.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-6.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-7.jpg" alt="final renders" size="100%" >}}

{{< youtube QqJkFtsO5Lg >}}

<h2 style="color: rgb(164, 30, 118); font-size: 70px; margin-bottom: 10px ">5</h2>

{{<image-center src="other1.jpg" alt="game" size="100%" >}}

{{< box link="toz" no="box-toz" title="TÖZ" subtitle="Shamanic Prop Design" image="toucan-toz.png" hoverImage="toz-hover.jpg" alt="toz toucan" >}}
3D Modeling, 3D Rendering, Concept Art / 2021
{{< /box >}}

{{< box link="orbit" no="box-orbit" title="ORBIT" subtitle="Tabletop Gaming for 2032" image="toucan-orbit.png" hoverImage="orbit-hover.jpg" alt="orbit toucan" >}}
3D Animation, 3D Rendering, Industrial Design / 2022
{{< /box >}}

{{< box link="matchbox" no="box-matchbox" title="MATCHBOX" subtitle="Lighting & Power Unit" image="toucan-ignite.png" hoverImage="matchbox-hover.jpg" alt="matchbox toucan" >}}
3D Modeling, 3D Rendering, Industrial Design / 2021
{{< /box >}}

