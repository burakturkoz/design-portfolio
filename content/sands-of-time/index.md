+++
title="Sands of Time"
+++

{{< project-subtitle text="Alternative Exhibition Space for the Rosetta Stone" >}}

{{< project-type-subtitle text="// Exhibition Design / Spatial Experience //" >}}

{{< youtube QqJkFtsO5Lg >}}

## OVERVIEW

The Rosetta Stone is a famous historical artifact that played a pivotal role in uncovering the secrets of the Ancient Egyptian hieroglyphs. Today, it is exhibited in the British Museum. However in my opinion, it's current exhibition space does not give the artifact the necessary importance and spotlight it deserves. I decided to take on the challenge of designing an alternative exhibition space for it, which will provide the visitors with an unforgettable experience.

**Project Type:** Individual work

**Course:** Directed Projects in Design II, Middle East Technical University

**Year:** 2022

## RESEARCH

{{< color-title-sands text="ROSETTA STONE" >}}

{{<image src="sands-rosetta.jpg" alt="rosetta stone research" size="100%" >}}


{{< color-title-sands text="CASE STUDIES: MUSEUM EXPERIENCES" >}}

I looked into existing museums to analyze they provide the viewevs with memorable experiences.
{{<image src="sands-museums.jpg" alt="museum studies" size="100%" >}}

----

{{< color-title-sands text="HOW TO CREATE SPATIAL EXPERIENCES?" >}}

{{<image src="sands-research.jpg" alt="experience research" size="100%" >}}

## IDEATION

{{<image src="sands-sketches.jpg" alt="sketches" size="100%" >}}

## FINAL RENDERS

{{<image src="sands-f-1.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-2.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-3.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-4.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-5.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-6.jpg" alt="final renders" size="100%" >}}

{{<image src="sands-f-7.jpg" alt="final renders" size="100%" >}}