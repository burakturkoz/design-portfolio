+++
title="Töz"
+++

{{< project-subtitle text="Designing a Movie Prop Based on the Ancient Shamanic Totem, Töz" >}}

{{< project-type-subtitle text="// Prop Design for Movie / Concept Design //" >}}

{{<image src="toz-cover.jpg" alt="toz cover" size="100%" >}}

## OVERVIEW

In the shamanic beliefs of many Central Asian Turkic cultures, the concept of "Töz" plays a very important role. Being the protector spirit of the tribe, "Töz" was an idol often taking the form of a human or animal ancestor. It was featured in many religious rituals and was held in high regard. 

As a design challenge, I wondered what a "Töz" would look like if it was featured in a movie made today. Although there are various existing examples of "Töz" idols, they would not communicate much to modern movie audiences. I wanted to design a concept for it that would have an impact on modern audiences, while staying true to the academic and historical facts about it's meaning and origins.

**Project Type:** Individual work

**Course:** Directed Projects in Design I, Middle East Technical University

**Year:** 2021

## RESEARCH

{{< color-title-toz text="LITERATURE SEARCH" >}}

{{<image src="toz-research.jpg" alt="toz research" size="100%" >}}

## IDEATION

{{< color-title-toz text="SKETCHES" >}}

{{<image src="toz-sketch.jpg" alt="toz sketches" size="100%" >}}

## FINAL DESIGN

{{<image src="toz-final.jpg" alt="toz final renders" size="100%" >}}


