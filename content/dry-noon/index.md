+++
title="Dry Noon"
+++

{{< project-subtitle text="A Game of Quickdraw with Hairdryers" >}}

{{< project-type-subtitle text="// Digital Fabrication //" >}}

{{< youtube ECl6DoxhCsU >}}

## OVERVIEW

Dry Noon is a two-player game that uses Arduino-powered components. It was inspired by cowboy duels from western movies, but instead of guns I chose to use hairdryers. You play as cowboys with stylish hair, trying to "shoot off" each other's hair down to win. 

This project was my first experience with programming and using Arduino's with other electrical components such as IR transmitters & receivers.

**Project Type:** Individual work

**Course:** Physical Computing, Aalto University

**Year:** 2022

## PROCESS

{{< color-title-dry text="PRELIMINARY SKETCHES" >}}

The basic idea when starting the project was this: The controller for the game is a hairdryer. To "shoot", you have to point it at your opponent and press the "trigger". The players wear helmets that have amazing hairstyles on it. 

Whoever points the hairdryer first and shoots, wins the quickdraw. The opponent's hair flies off and their fashion sense is ruined.

{{<image src="dry-sketch.jpg" alt="sketches" size="100%" >}}

{{< color-title-dry text="IR TRANSMITTER AND RECEIVER" >}}

I first tested how the IR receiver works, and how the library is set up for Arduino. For this, I used the IR Control Kit by SparkFun. It was my first time using this kit.

{{<video src="output.mp4">}}Testing the IR receiver with p5.js{{</video>}}

After I got the transmitter working, I set up the receiver by following the SparkFun tutorial. I made it so that when the button press is detected, the transmitter sends out a signal. If the receiver picks it up, "BANG" is printed in the console.

Finally, I hooked up a servo to the system, so that when the receiver picks up the signal, a motor is turned. This was to simulate the "shooting off the hair", since I wanted to use a servo to accomplish that.

{{<image src="i1.jpg" alt="ir breadboard" size="49%" >}}
{{<image src="i2.jpg" alt="ir breadboard" size="38%" >}}

Here is the working code:

{{< detail-tag "CODE: Arduino IDE" >}}

```go
#include <Servo.h>
#define DECODE_NEC          // Includes Apple and Onkyo
#define DECODE_DISTANCE     // in case NEC is not received correctly
#include <IRremote.hpp>
Servo myServo;

uint16_t sAddress = 0x0102;
uint8_t sCommand = 0x34;
uint8_t sRepeats = 1;

int btnPin = 12;
int btnState = 0;
int prevBtnState = 0;
int servoPos = 0;

// Transmitter P3
// Receiver P2
// Button P12
// Servo P9

void setup() {
    pinMode(btnPin,INPUT);
    Serial.begin(115200);
    // Start the receiver and if not 3. parameter specified, take LED_BUILTIN pin from the internal boards definition as default feedback LED
    IrReceiver.begin(2, ENABLE_LED_FEEDBACK);
    IrSender.begin(3,ENABLE_LED_FEEDBACK);
    myServo.attach(9);
}

void send_ir_data() {
    Serial.print(F("Sending: 0x"));
    Serial.print(sAddress, HEX);
    Serial.print(sCommand, HEX);
    Serial.println(sRepeats, HEX);

    // clip repeats at 4
    if (sRepeats > 4) {
        sRepeats = 4;
    }
    // Results for the first loop to: Protocol=NEC Address=0x102 Command=0x34 Raw-Data=0xCB340102 (32 bits)
    IrSender.sendNEC(sAddress, sCommand, sRepeats);
}

void receive_ir_data() {
    if (IrReceiver.decode()) {
        Serial.print(F("Decoded protocol: "));
        Serial.print(getProtocolString(IrReceiver.decodedIRData.protocol));
        Serial.print(F("Decoded raw data: "));
        Serial.print(IrReceiver.decodedIRData.decodedRawData, HEX);
        Serial.print(F(", decoded address: "));
        Serial.print(IrReceiver.decodedIRData.address, HEX);
        Serial.print(F(", decoded command: "));
        Serial.println(IrReceiver.decodedIRData.command, HEX);
          if(IrReceiver.decodedIRData.command==0x34){
            Serial.println("BANG!");
            turnServo();
          }
        IrReceiver.resume();
    }
}

void turnServo(){
    myServo.write(180);
    delay(3000);
    myServo.write(0);
}

void loop() {
    /*
     * Print loop values
     */

     /*
    Serial.println();
    Serial.print(F("address=0x"));
    Serial.print(sAddress, HEX);
    Serial.print(F(" command=0x"));
    Serial.print(sCommand, HEX);
    Serial.print(F(" repeats="));
    Serial.println(sRepeats);
    Serial.flush();
    */

    btnState=digitalRead(btnPin);

    if(btnState != prevBtnState){
      if(btnState==HIGH){
        send_ir_data();
        Serial.println("button pressed");
      }
    }

    prevBtnState=btnState;

    delay(10);

    // wait for the receiver state machine to detect the end of a protocol
    receive_ir_data();

    // Prepare data for next loop
    //sAddress += 0x0101;
    //sCommand += 0x11;
    //sRepeats++;

}
```

{{< /detail-tag >}}

{{< color-title-dry text="HARDWARE DESIGN" >}}

After getting the code to work, I started thinking about where and how the components will be placed.

At first, I was thinking to put the Arduino and the board in the hairdryer or on the headpiece. But then I realized both of these pieces were constantly being moved around or interacted with in my scenario.

So I decided to make a seperate case for them which would rest on the belt, and be connected with cables. The connections are made with 3.5 mm stereo jacks, which allow for three cables allocated for positive, ground and signal.

{{<image src="dry-chart.jpg" alt="hardware chart" size="100%" >}}

I wanted to place all the electronic components inside cases. Since this product will be something that people will constantly interact with, the electronics need to be protected.

{{<image src="dry-cad.jpg" alt="cad models" size="100%" >}}

{{<image src="i3.jpg" alt="3d printed case" size="38.7%" >}}
{{<image src="i4.jpg" alt="3d printed case" size="30%" >}}
{{<image src="i5.jpg" alt="3d printed case" size="30.5%" >}}

{{< color-title-dry text="HEADPIECE" >}}

For the headpiece, I needed something that can hold a servo and IR receiver on it. I used a wig and steel wires to construct a structure on which these components will be placed.

{{<image src="i6.jpg" alt="3d printed case" size="47%" >}}
{{<image src="i7.jpg" alt="3d printed case" size="48%" >}}

{{< color-title-dry text="BRINGING IT ALL TOGETHER" >}}

The final stage was bringing all the electronic and mechanic components together. I soldered and placed the electronics inside the 3D printed cases, tested in each stage of the process.

{{<image src="i8.jpg" alt="3d printed case" size="43%" >}}
{{<image src="i9.jpg" alt="3d printed case" size="55%" >}}

{{<video src="output2.mp4">}}Testing the servo with a button{{</video>}}

After making sure that the servo was working, I moved on to testing the IR receiver and transmitter was working correctly. Finally, I placed the button and transmitter inside a hairdryer.

{{<video src="output3.mp4">}}Testing IR{{</video>}}

{{<video src="output4.mp4">}}Testing IR with hairdryer casing{{</video>}}

## COMPLETED PROTOTYPE

{{< color-title-dry text="EXHIBITION" >}}

After I got them all together, I had completed a working prototype for my Physical Computing course, and I exhibited it.

{{<image src="i10.jpg" alt="exhibition" size="51%" >}}
{{<image src="i11.jpg" alt="exhibition" size="48%" >}}

{{<image src="i12.jpg" alt="exhibition" size="100%" >}}

{{< color-title-dry text="WHAT'S NEXT?" >}}

As I intend this to be a two-person game, I will produce one more pair. After this is completed, two people will be able to play against each other in a game of quickdraw.
