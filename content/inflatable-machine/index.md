+++
title="Inflatable Making Machine"
+++

{{< project-subtitle text="Fab Academy- Machine Building Assignment" >}}

{{< project-type-subtitle text="// Digital Fabrication //" >}}

{{<video src="aalto-video.mp4">}}{{</video>}}

## OVERVIEW

The purpose of this machine is to make inflatables by welding two plastic sheets together. We combined ultrasonic welder and a 3-axis machine kit, and designed custom parts around them. This was a group project for Fab Academy's machine building assignment. The group repository can be accessed [here](https://gitlab.fabcloud.org/academany/fabacademy/2024/labs/aalto/group-assignments/machine-building).

**Project Type:** Group work

**Group Members** Burak Türköz, [Sara Kutkova](https://fabacademy.org/2024/labs/aalto/students/sara-kutkova/), [Serdar Yalçın](https://fabacademy.org/2024/labs/aalto/students/sara-kutkova/)

**Course:** Fab Academy

**Year:** 2024

## BUILDING PROCESS

{{< color-title-inflatable text="THE IDEA" >}}

The main goal behind this machine is to simplify the process of making inflatables. A common way to make them is to melt two sheets of thermoplastics and stick them to each other (e.g. EVA film). This can be done by hand using a soldering iron, or by using a 3D printer nozzle. Both methods have their own downsides and are not ideal for larger, accurate manufacturing needs.

The solution we wanted to try was to use an [ultrasonic welder](https://www.amazon.com/BAOSHISHAN-Ultrasonic-Plastic-Collecting-Conveyor/dp/B08DV358X8/ref=sr_1_7?crid=6N0QPW8SLO44&keywords=ultrasonic+welder+plastic&qid=1679017354&sprefix=ultrasonic+welder+plasti%2Caps%2C94&sr=8-7) and attach it to a 3-axis machine with a custom tool holder.

{{<image src="i1.1.jpg" alt="ultrasonic welder" size="60%" >}}
{{<image src="i1.2.jpg" alt="ultrasonic welder" size="39%" >}}

{{< color-title-inflatable text="ASSEMBLING THE OPENBUILDS ACRO SYSTEM" >}}

The system we used for the base of the machine was a [OpenBuilds Acro 1010](https://openbuildspartstore.com/openbuilds-acro-1010-40-x-40/). It allows for movement along X-Y-Z axes with stepper motors. This provides a base for any end effector to be attached, such as a diode laser or pen holder. The system also has it's own software, which allows us to directly send G-Code or control the motors manually.

{{<image src="i1.jpg" alt="openbuilds acro" size="48%" >}}
{{<image src="i2.jpg" alt="openbuilds acro" size="48%" >}}

{{<image src="i5.jpg" alt="openbuilds acro" size="48%" >}}
{{<image src="i4.jpg" alt="openbuilds acro" size="48%" >}}

{{< color-title-inflatable text="DESIGNING THE END EFFECTOR" >}}

All of the group members worked on assembling the machine, but we also had our individual tasks. Mine was to design and build the end effector to attach the ultrasonic welder. It was supposed to hold the welder and Z-axis stepper motor, and enable vertical movement. 

We first experimented with using lead screws. We needed a lead nut for it but we did not have it in the Fablab inventory. Thus, we tried to model and 3D print it.

{{<video src="output1.mp4">}}3D printing lead nut{{</video>}}

However, it did not fit the screw we had. We realized it would be very time consuming to go with this strategy.

{{<image-center src="i8.jpg" alt="lead nut" size="48%" >}}

Therefore, we disassembled an old 3D printer and took away it's lead nut.

{{<image src="i6.jpg" alt="lulzbot" size="48%" >}}
{{<image src="i10.jpg" alt="lulzbot" size="48%" >}}

{{<image src="i7.jpg" alt="lulzbot" size="48%" >}}
{{<image src="i11.jpg" alt="lulzbot" size="48%" >}}

Now that we were going to use this lead nut, I designed an end effector around it. I measured the welder and modeled the first prototype assembly.

{{<video src="output10.mp4">}}End Effector Model{{</video>}}

{{<image src="i12.jpg" alt="end effector" size="48%" >}}
{{<image src="i14.jpg" alt="end effector" size="48%" >}}

{{<video src="output2.mp4">}}End Effector Test{{</video>}}

I also laser cut a new mounting panel, since the default one was too short for this end effector.

{{<image src="i13.jpg" alt="end effector" size="48%" >}}
{{<image src="i16.jpg" alt="mounting panel" size="48%" >}}

{{<image src="i17.jpg" alt="mounting panel" size="48%" >}}
{{<image src="i18.jpg" alt="assembled" size="48%" >}}

Finally, we used foam and zip-ties to hold the welder in place. In the second iteration, a more permanent solution will be implemented for this.

{{<image src="i20.jpg" alt="holding" size="48%" >}}
{{<image src="i21.jpg" alt="holding" size="48%" >}}

{{< color-title-inflatable text="TESTING THE MACHINE" >}}

When everything was assembled, we moved on to testing the machine. We used two EVA sheets as our material, with baking paper on top to prevent burning.

{{<image src="i24.jpg" alt="testing" size="48%" >}}
{{<image src="i23.jpg" alt="testing" size="48%" >}}
{{<image src="i25.jpg" alt="testing" size="48%" >}}
{{<image src="i26.jpg" alt="testing" size="48%" >}}

Our first tests failed and the plastic started to rip. This was due to the effector being positioned too low. We experimented with different Z-heights and welding settings.

{{<video src="output3.mp4">}}Failed test{{</video>}}

{{<video src="output4.mp4">}}Succesfull test{{</video>}}

{{<video src="output6.mp4">}}Testing welding strength{{</video>}}

{{< color-title-inflatable text="FINAL RESULT" >}}

After getting the settings down, we decided to make an inflatable flamingo. 

{{<video src="output9.mp4">}}Welding the flamingo{{</video>}}

{{<video src="output8.mp4">}}Testing the flamingo{{</video>}}

{{<image-center src="i27.jpg" alt="final" size="100%" >}}
{{<image-center src="i28.jpg" alt="final" size="100%" >}}

In this stage, the machine works reliably to create inflatables out of EVA sheet. I also designed a second iteration of the end effector, correcting some mistakes from the old one. In the future versions, the end effector will be updated to feature a changeable tool holder. 