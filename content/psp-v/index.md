+++
title = "PSP: V"
+++

{{< project-subtitle text="A Re-Design of Sony's Handheld Gaming Console PSP, for the 2023 Market" >}}

{{< project-type-subtitle text="// Product Design //" >}}

{{<image src="psp-1.jpg" alt="psp cover image" size="100%" >}}

## OVERVIEW

Since the PlayStation Portable (PSP) ended it's development in the early 2010's, Sony has not developed a handheld gaming console. We decided to explore the possibility of what it could look like if the company decided to develop a handheld for the 2023 market. PSP: V is a re-design of the PSP, developed to compete with the likes of Nintendo Switch. 

**Project Type:** Group work

**Team:** Burak Türköz & Evren Uçar

**My Role:** The ideation & design process was a joint effort. At the very end, I was responsible for CAD modeling and Evren was respnosible for rendering.

**Course:** Industrial Design Studio V, Middle East Technical University

**Year:** 2021

## RESEARCH

{{< color-title-psp text="GENERAL INFORMATION" >}}

"Play-Station Portable" or "PSP" for short, is Sony's handheld gaming console released between 2004-2014 as different models and iterations. The specific model we decided to re-design for the 2023 market was the "PSP 2000", originally released in 2007.
{{<image src="psp-info-3.jpg" alt="general info" size="100%" >}}

----

{{< color-title-psp text="MARKET SEGMENTATION" >}}

We had to decide for what market segment we would design our product. Our aim was to carry the new "re-designed PSP" upwards in the market segment to be a direct competitor for the Nintendo Switch.
{{<image src="psp-market.jpg" alt="market segmentation" size="100%" >}}

----

{{< color-title-psp text="USER EXPERIENCE TESTING" >}}

To help us understand which parts of the original PSP we had to change, and what parts to keep; we conducted user testing sessions. We asked our participants to physically interact with the device and give us feedback on various aspects of it.
{{<image src="psp-user.jpg" alt="user testing" size="100%" >}}

## IDEATION

{{< color-title-psp text="SKETCHING" >}}

With the feedback we got from our tests, we started ideating on designs. We placed them on an axis ranging from appealing to a more casual user-base, to hardcore gamers.
{{<image src="psp-sketch.jpg" alt="skecthes" size="100%" >}}

----

{{< color-title-psp text="USER PROFILE" >}}

After initial ideation, we decided on a sample user persona that would guide us for the rest of the design process.
{{<image src="psp-user-profile.jpg" alt="user-profile" size="100%" >}}

----

{{< color-title-psp text="DESIGN ALTERNATIVES" >}}

We reduced our design alternatives to three design directions, and created low fidelity mock-ups to test their ergonomic viability.
{{<image src="psp-designs.jpg" alt="design alternatives" size="100%" >}}

----

{{< color-title-psp text="BRAND IDENTITY CONSTRAINTS" >}}

In line with Playstation’s brand identity, the thumbsticks must be below the control buttons and be symmetrical. Although the 45 degree angle is comfortable for conventional controllers, it is harder to replicate in handhelds.

{{<image src="psp-brand.jpg" alt="brand identity" size="100%" >}}

----

{{< color-title-psp text="FINALIZATION" >}}

Our solution to the brand-identity/ ergonomics clash was to use the "folding screen" technology. This way, we could keep the device relatively compact when carried, but offer a full-sized display when playing. We experimented with CAD mock-ups and refined the form, color and materials.

{{<image src="psp-finalization.jpg" alt="finalization" size="100%" >}}

## FINAL PRODUCT

{{<image src="psp-final.jpg" alt="final board" size="100%" >}}