+++
title="'Making a Mobile Makerspace': Master's Thesis Project"
+++

{{< project-subtitle text="Designing, Building and Testing a Mobile Digital Fabrication Service for Children's Education" >}}

{{< project-type-subtitle text="// Makerspace Design/ Maker Education //" >}}

{{<image-center src="i22.jpg" alt="cabinets with machines" size="100%" >}}

## OVERVIEW

Makerspaces are shared workshops where artifacts are produced in a creative and collaborative manner by using digital fabrication equipment like 3D printers and laser cutters. “Mobile makerspaces”, which can be defined as maker locations that move, aim to increase public access to these creative spaces. One of their use cases is providing STEAM education to children by bringing maker equipment to schools. 

The goal of this thesis is to create a proof-of-concept for a mobile makerspace that would operate in Finland’s Uusimaa region. The primary use case is providing maker education to children through digital fabrication tools. The thesis documents the design, building, and testing process of a mobile makerspace. The prototype consists of two components: physical and educational. The former refers to transportation cabinets: physical products for carrying digital fabrication machines. The latter refers to a sample educational workshop to be conducted by the mobile makerspace. For both components, the design process includes background research in the form of interviews and literature review. 

The project was created in partnership with Aalto University’s center for youth education, [Aalto Junior](https://www.aalto.fi/en/aalto-university-junior), which provided machines, funding, and the means to conduct a test workshop.

As of 2025, I am developing an open-source parametric tool that will allow people to create create CAD files for a cabinet with custom dimensions that can fit their own machines. The design is called "Open Box", and will be made available soon.

**Project Type:** Individual work

**Course:** Master's Thesis Project, Department of Art and Media, Aalto University

**Year:** October 2023 - November 2024

<a href="/design-portfolio/docs/making-a-mobile-makerspace-turkoz-burak-2024.pdf" download="making-a-mobile-makerspace-turkoz-burak-2024.pdf" 
   style="display: block; 
          width: fit-content; 
          margin: 20px auto; 
          padding: 20px; 
          text-align: center; 
          text-decoration: none; 
          color: #000; 
          border: 3px solid #353535; 
          border-radius: 5px; 
          box-shadow: 0 2px 2px rgba(0, 0, 0, 0.3); 
          font-family: 'Golos Text', sans-serif; 
          font-weight: 500; 
          transition: background-color 0.3s ease, color 0.3s ease;"
   onmouseover="this.style.backgroundColor='rgb(241, 129, 44)'; this.style.color='#fff';" 
   onmouseout="this.style.backgroundColor=''; this.style.color='#000';">
  Download Full Text
</a>

## PROTOTYPING

This page includes documentation of the mobile makerspace's prototyping process. The background research, interviews, and analysis that took place before prototyping can be found in the thesis text.

{{< color-title-mobile-lab text="USAGE SCENARIO" >}}

The use case for the mobile makerspace was chosen as children’s education through digital fabrication and making activities. Schools, libraries, and youth education centers in Finland’s Uusimaa region would be potential venues. Aalto Junior purchased a laser cutter, a 3D printer and a laser cutter for me to use for this thesis. The usage scenario and the prototypes were designed with these machines in mind. 

{{<image src="thesisplan1.jpg" alt="thesisplan1" size="100%" >}}

Compact digital fabrication machines are placed in specialized movable cabinets.

{{<image src="thesisplan2.jpg" alt="thesisplan1" size="100%" >}}

The machines are placed in the back of a van and transported to a school to give a workshop. The workshop locations are chosen among institutions that don't have ready access to this equipment.

{{<image src="thesisplan3.jpg" alt="thesisplan1" size="100%" >}}

Digital fabrication workshops are given to various age groups. In addition to the students receiving maker education, the teachers are also informed about how they can utilize these technologies.

{{< color-title-mobile-lab text="FIRST ITERATION" >}}

I first started by sketching the transportation cabinets. The first goal was for a single cabinet to carry one machine and all materials related to it (e.g. plywood, filament, computers). Thus, each cabinet would have a machine compartment and a material compartment. 

Another core idea was that the cabinets would transform into working spaces when stationary. This stemmed from the use case of giving workshops at schools. Instead of re-arranging the classrooms in schools, the cabinets having their own workspace would decrease the set-up time considerably. 

{{<image src="sketches.jpg" alt="sketches" size="49%" >}}

{{<image src="sketches2.jpg" alt="sketches" size="49%" >}}

In April 2024, I manufactured and assembled the first protype of the transportation cabinets for the laser cutter.

{{<video src="output3.mp4">}}CAD model of first iteration{{</video>}}

{{<video src="output5.mp4">}}CNC milling{{</video>}}

{{<image src="i1.jpg" alt="prototypes" size="49%" >}}
{{<image src="i2.jpg" alt="prototypes" size="49%" >}}

{{< color-title-mobile-lab text="SECOND ITERATION" >}}

After the first prototype, I adjusted the design and manufactured the second box for the vinyl cutter. I improved the door closing mechanism and added supports to the top to hold up the side panels when opened.

{{<video src="output4.mp4">}}CAD model of second iteration{{</video>}}

{{<image src="i4.jpg" alt="prototypes" size="49%" >}}
{{<image src="i5.jpg" alt="prototypes" size="49%" >}}

{{<image src="i3.jpg" alt="prototypes" size="49%" >}}
{{<image src="i12.jpg" alt="prototypes" size="49%" >}}

I added the same modifications to the laser cutter cabinet as well.

{{<image src="i6.jpg" alt="prototypes" size="49%" >}}
{{<image src="i7.jpg" alt="prototypes" size="49%" >}}

{{<image src="i8.jpg" alt="prototypes" size="49%" >}}
{{<image src="i9.jpg" alt="prototypes" size="49%" >}}


{{<image src="i15.jpg" alt="cabinets with machines" size="49%" >}}
{{<image src="i16.jpg" alt="cabinets with machines" size="49%" >}}

{{< color-title-mobile-lab text="PORTABILITY & USABILITY TESTS" >}}

As the next step, I wanted to test the following:

1. How easy is it to move the cabinets?
2. Are the machines easy to use when placed in the cabinets?
3. Can the cabinets be easily loaded to a van and transported?

Below are photos from the moving test. Originally, I designed one cabinet to be pushed or pulled by one person. However, I realized two cabinets can also be moved together by one person, when straps are used.

{{<image src="i13.jpg" alt="moving test" size="49%" >}}
{{<image src="i14.jpg" alt="moving test" size="49%" >}}

{{<video src="output1.mp4">}}One person moving two cabinets{{</video>}}

Here is how the cabinets look when the machines are placed inside. No major problems were encountered at this stage. Machines can be easily placed inside the cabinet, and all controls are unobstructed. However, machines were not operated at this stage, more tests on this will be conducted during the workshop in June.

{{<image-center src="i18.jpg" alt="cabinets with machines" size="100%" >}}

{{<image-center src="i19.jpg" alt="cabinets with machines" size="100%" >}}

{{<image-center src="i20.jpg" alt="cabinets with machines" size="100%" >}}

{{<image-center src="i21.jpg" alt="cabinets with machines" size="100%" >}}

{{<image-center src="i22.jpg" alt="cabinets with machines" size="100%" >}}

Finally, the cabinets were loaded into a van. Two people were easily able to lift one box. Afterwards, the van was driven around the campus; the cabinets were stable throughout the journey. However, machines were not inside the cabinets during this test. I first wanted to ensure that loading and transporting the cabinets alone went smoothly. A separate test will be conducted in the future with the machines inside.

{{<image src="i23.jpg" alt="loading test" size="49%" >}}
{{<image src="i24.jpg" alt="loading test" size="49%" >}}

{{<image src="i25.jpg" alt="loading test" size="49%" >}}
{{<image src="i26.jpg" alt="loading test" size="49%" >}}

{{< color-title-mobile-lab text="WORKSHOP DESIGN" >}}

As I was building the cabinets, I was also working on the workshop design. The main idea is to design a sample workshop that can be given with the mobile makerspace. Through interviews and literature research, I outlined guidelines regarding pedagogical strategies. These strategies helped me shape the workshop design. This part is explained in-detail in the thesis.

The goal of the workshop is to design and build custom miniature houses out of plywood. The laser cutter will be used to make the houses. In the second part, they will make stickers with a vinyl cutter and decorate their buildings. Like this example:

{{<image src="i27.1.JPG" alt="example houses" size="100%" >}}

I decided to use [cuttle.xyz](cuttle.xyz) as software for the workshop. The first reason why I chose Cuttle is the simplicty of it's user interface. The basics of the software can be easily taught to children in a short workshop. The second reason was because it allows for parametric design.

The need for parametric design came from the pedagogical strategy guidelines. I wanted the participants to have the freedom of creating their own designs. However, the workshop aimed at children who have no knowledge of the software being used. Coupled with the fact that the workshop would have to fit in 4 hours, teaching how to create custom designs becomes a challenge. 

To combat this, I created a [parametric design in Cuttle](https://cuttle.xyz/@burakturkoz/parametric-house-P92PnJDg3vd1) that allows for easy size adjustment. This way, the dimensions of the house can be changed with one click, and the design automatically adapts. The participants will be ensured to have a house that fits together and not have to worry about advanced details like "kerf adjustment". Instead, they will focus on creating the doors & windows and decorative elements like text or images.

{{<video src="output2.mp4">}}Parametric house builder in cuttle{{</video>}}

At the end of the workshop, all the houses will be brought together to form a town. We will be using painted tiles with forests and rivers on it, to ground the experience. To add more decoration and play elements, I created some trees for this landscape. In this process, I used [comb test made easy](https://cuttle.xyz/@lu_u/comb-test-made-easy-9knZ2wZeQKZM), a parametric tool designed in Cuttle by [Lù Chen](https://l-lu-u.github.io/). The tool helps determine laser cutting settings and is very helpful especially for large batch cuts like this.

Alongside the trees, I did quite a lot of test cuts to ensure that the house design works well and there are no issues during the workshop.

{{<image src="i10.jpg" alt="prototypes" size="49%" >}}
{{<image src="i11.jpg" alt="prototypes" size="49%" >}}

{{<image src="i27.jpg" alt="workshop materials" size="49%" >}}
{{<image src="i30.jpg" alt="workshop materials" size="49%" >}}

{{<image src="i29.1.jpg" alt="workshop materials" size="49%" >}}
{{<image src="i29.jpg" alt="workshop materials" size="49%" >}}

## TEST WORKSHOP
{{< color-title-mobile-lab text="DESIGN YOUR OWN HOUSE!" >}}

The test workshop was held as a part of Aalto Junior's summer school program. 14 children aged between 10-12 participated in total. I led the workshop and was helped by three assistants. The workshop lasted around 4 hours, excluding the breaks.

Participants were first given a brief introduction to the software. Then, they started exploring and designing their houses. Everyone was encouraged to design with a concept in mind (e.g. a bakery).

{{<image src="i31.JPG" alt="workshop pictures" size="49%" >}}
{{<image src="i32.JPG" alt="workshop pictures" size="49%" >}}

{{<image-center src="i33.JPG" alt="workshop pictures" size="100%" >}}

The laser cutter and vinyl cutter were placed in the mobile makerspace cabinets I built. In addition to being transportation structures, they functioned as workspaces. The sides were used as a desk to place a computer and other materials. The bottom shelves were used to store extra plywood and vinyl.

{{<image-center src="i34.JPG" alt="workshop pictures" size="100%" >}}
{{<image-center src="i35.1.JPG" alt="workshop pictures" size="100%" >}}
{{<image-center src="i36.JPG" alt="workshop pictures" size="100%" >}}

{{<image src="i35.JPG" alt="workshop pictures" size="49%" >}}
{{<image src="i37.JPG" alt="workshop pictures" size="49%" >}}

{{< color-title-mobile-lab text="ASSEMBLING" >}}

Once their designs were completed, I collected and laser cut them. In the afternoon, they assembled their houses.

{{<image src="i38.JPG" alt="workshop pictures" size="49%" >}}
{{<image src="i39.JPG" alt="workshop pictures" size="49%" >}}

{{<image src="i40.JPG" alt="workshop pictures" size="49%" >}}
{{<image src="i41.JPG" alt="workshop pictures" size="49%" >}}

{{< color-title-mobile-lab text="DECORATING" >}}

After this, I showed them how to prepare designs for the vinyl cutter. We decorated their houses with stickers.

{{<image src="i42.JPG" alt="workshop pictures" size="49%" >}}
{{<image src="i43.JPG" alt="workshop pictures" size="49%" >}}

{{< color-title-mobile-lab text="FINAL RESULTS" >}}

In the end, everyone brought their houses together to build a town. The end result from each participant was unique. They were all able to design and create custom buildings with the skills they learned in the workshop. 

{{<image-center src="i45.JPG" alt="workshop pictures" size="100%" >}}
{{<image-center src="i46.JPG" alt="workshop pictures" size="100%" >}}
{{<image-center src="i47.JPG" alt="workshop pictures" size="100%" >}}
{{<image-center src="i50.JPG" alt="workshop pictures" size="100%" >}}
{{<image-center src="i49.JPG" alt="workshop pictures" size="100%" >}}
{{<image-center src="i48.JPG" alt="workshop pictures" size="100%" >}}
