+++
title="Orbit"
+++

{{< project-subtitle text="Board Gaming System for the Year 2032" >}}

{{< project-type-subtitle text="// Product Design / Concept Design //" >}}

{{< youtube g_SHUggaerc >}}

## OVERVIEW

Orbit is a concept design project for the near future, aiming to utilize emerging technologies to create a home entertainment system for the year 2032. We decided to approach the topic of home entertainment through board games. In a world which virtual entertainment is becoming more dominant, we aimed to design a system that encourages socialization through physical get-togethers.

**Project Type:** Group work

**Team:** Bora Özden, Burak Türköz, Ilgaz Öz, Yiğit Karahan

**My Role:** The ideation process was a joint effort in our group. My specialized work area was sketching our product ideas, and creating the final animation you see above.

**Course:** Industrial Design Studio V, Middle East Technical University

**Year:** 2022

## IDEATION

{{< color-title-orbit text="PRELIMINARY SKETCHES" >}}

Our approach to the idea of board gaming in the future was combining physical elements like figures and in-person interactions, with digital elements like touch-enabled projections. We focused on protecting the charm of tabletop games being played in a common physical space.
{{<image src="orbit-sketches.jpg" alt="preliminary sketches" size="100%" >}}

----

{{< color-title-orbit text="USER PROFILE" >}}

We created a user profile that focused on specific aspects of tabletop gaming that we wanted to emphasize in our system.
{{<image src="orbit-user.jpg" alt="user profile" size="100%" >}}

----

{{< color-title-orbit text="ITERATIONS: PROJECTOR" >}}

{{<image src="orbit-iterations-p.jpg" alt="projector iterations" size="100%" >}}

----

{{< color-title-orbit text="ITERATIONS: TRINKET" >}}

{{<image src="orbit-iterations-t.jpg" alt="trinket iterations" size="100%" >}}

----

## FINAL PRODUCT

{{<image src="orbit-final.jpg" alt="final product" size="100%" >}}