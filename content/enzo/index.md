+++
title="Enzo the Animatronic Toucan"
+++

{{< project-subtitle text="Fab Academy Final Project" >}}

{{< project-type-subtitle text="// Digital Fabrication //" >}}

{{< youtube 3lB0ZQUqhuU >}}

## OVERVIEW

Enzo is an animatronic bird that I designed and built in Aalto Fablab. He is powered by two stepper motors driven by a custom-made PCB and controlled with a wireless network interface. Every part of the assembly was made using the digital fabrication techniques I practiced throughout Fab Academy, including: PCB design & milling, laser cutting, 3D CNC milling and vacuum forming. The software side includes embedded programming of a Xiao ESP32C3 and a network interface using jQuery & Bootstrap.

Enzo's head can move at a custom speed and angle, controlled by using steppers mounted on the same plane, and a universal joint. This fundamental system of moving an animatronic was inspired by an existing design by [Jasper Anderson](http://www.jasperjanderson.com/p/special-effects.html). I built a similar system, then designed Enzo and his every detail from the ground up.

**Project Type:** Individual work

**Course:** Fab Academy

**Year:** 2024

## IDEATION

{{< color-title-enzo text="PRELIMINARY SKETCHES" >}}

To start, I researched some examples on how to animate objects using motors and mechanisms. During my research, I came across animatronics projects. This inspired me to create something similar, building upon my previous ideas. 

I still wanted to do something that has character. My first idea was to make a sock puppet animatronic that would react to it's surroundings. After looking at some examples, this idea turned into making an animal animatronic. More specifically, I decided that I would make a toucan, which is my [design portfolio mascot](https://burakturkoz.gitlab.io/design-portfolio/).

Here are some similar projects I found along the way. I've looked into them and picked up some features from each.

[Animatronic raven DIY kit](https://www.youtube.com/watch?v=y0R8-F4TmPI&list=LL&index=5)

[How to achieve smoother movements with servo motors](https://www.youtube.com/watch?v=jsXolwJskKM&list=LL&index=6)

[Uncanny eye animatronic](https://www.youtube.com/watch?v=DxjTiZKzxVo)

[Uncanny eye animatronic 2](https://www.youtube.com/watch?v=Ftt9e8xnKE4&t=378s)

Just like in the animatronic DIY kit, I want to make my toucan have a similar mechanism. Instead of servo motors, I am thinking of using stepper motors to have greater control over the speed of the movements. This way, I can make movements that have more of a character.

{{<image-center src="toucan-v1.jpg" alt="toucan sketch" size="100%" >}}

Here is the plan that I want to go forward with. Depending on how much time I will spend on the project, I will advance through these stages:

1. Complete only the head and beak. Have the head controllable with a universal joint, and the beak open/ close. Cover & paint the head to make it look realistic.
2. Make the head react to stimuli such as following your hand, or biting when you put your finger in the mouth.
3. Complete the rest of the body and cover it with the same methods as the head. 
4. Animate some features of the body, such as a small flap of the wings or movement in the feet.

{{< color-title-enzo text="MOCK-UPS" >}}

I started by constructing a mock-up from cardboard. This allowed me to experiment with motor placement, different motor wheel sizes, wire configurations and where to drill holes in the head. It gave me a general idea of how the system works, as a proof of concept. After the lo-fi prototyping stage, I was convinced that this design had potential and was worth pursuing.

{{<image src="i1.jpg" alt="lo-fi" size="48%" >}}
{{<image src="i2.jpg" alt="motors" size="48%" >}}

{{<image src="i3.jpg" alt="motors" size="48%" >}}
{{<image src="i4.jpg" alt="motors" size="48%" >}}

{{<video src="output1.mp4">}}Manual Motor Turn{{</video>}}

## FIRST ITERATION

{{< color-title-enzo text="CONTROLLING STEPPER MOTORS WITH NETWORK INTERFACE" >}}

I designed and milled a custom PCB that controls stepper motors in the [output devices week](https://burakturkoz.gitlab.io/digital-fabrication-by-burak/assignments/output-devices/) of the course. It was utilizing a potentiometer to control the movements.

{{<video src="output2.mp4">}}Motor Turn with Potentiometer{{</video>}}

To have better control over the motor, I decided to utilize a network interface. I used the [interface and application programming week](https://burakturkoz.gitlab.io/digital-fabrication-by-burak/assignments/interface-and-application-programming/) to create a motor control system. It sends the motor right and left turn commands.

{{<video src="output3.mp4">}}Motor Turn with Network Interface{{</video>}}

{{<video src="output4.mp4">}}Motor Turn with Network Interface{{</video>}}

As the next step, I wanted to have precise control over the angle and speed of movements, I modified the interface accordingly. By asking ChatGPT, I learned how to create sliders and get values from them in jQuery. I created two sliders: one for angle of movement and one for speed control. When I click the "turn" buttons, the values from the sliders gets sent to the XIAO.

{{< detail-tag "CODE: Arduino IDE" >}}

#### Arduino IDE

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

//#define STEPS 400

// Some variables we will need along the way
const char *ssid = "salvatore ferragamo";
const char *password = "macncheese";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;
int interval = 3000;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);

  Serial.begin(19200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int state;  // motor state
    int stepValue; // range slider value, which will be turned into stepCount
    float accelValue; // range slider value, which will be turned into accel

    if (request->hasParam("state")) {
      // The incoming params are Strings
      String param = request->getParam("state")->value();
      // .. so we have to interpret or cast them
      if (param == "CWturn") {
        state = 2;
      } else if (param == "CCWturn") {
        state = 3;
      } else {
        state = 0;
      }
    } else {
      state = 0;
    }

    if (request->hasParam("stepValue")){
      stepValue = request->getParam("stepValue")->value().toInt();
    }

    if (request->hasParam("accelValue")){
      accelValue = request->getParam("accelValue")->value().toFloat();
    }

     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement

    if (state == 2) {
      Serial.println("turningcw");
      digitalWrite(DIR_PIN, LOW);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCW";
    } else if (state == 3) {
      Serial.println("turningCCW");
      digitalWrite(DIR_PIN, HIGH);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCCW";
    } else {
      stateString = "notTurning";
    }

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}

void constantAccel(float accelVal){
  int delays[stepCount];
  float angle = 1;
  float accel = accelVal;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< stepCount; i++){
    float d = c0;
    if (i>0){
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    }      
    if (d<highSpeed){      
      d = highSpeed;
    }
      
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[i]);
    digitalWrite(STEP_PIN, LOW);
  }    

    for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[stepCount-i-1]);
    digitalWrite(STEP_PIN, LOW);
  }    
}

void loop() {

}
```
{{< /detail-tag >}}

{{< detail-tag "CODE: jQuery" >}}

#### jQuery
```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>BootStrap Rest DEMO</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>stepper control test</h1>
    <div class="form-group">
      <label for="rangeInput">Angle Input:</label>
      <input type="range" class="form-range" id="rangeInput" min="0" max="192" value="50"> 
    </div>
    <p>Selected value: <span id="rangeValue"> 50 </span></p>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="0.0001" max="0.03"  step="0.0001" value="0.01"> 
    </div>
    <p>Selected value: <span id="speedValue"> 0.01 </span></p>
    
    <div id="motor-sign">Motor Rotation</div>
    <button id="button-counter-clockwise" class="btn btn-warning">Turn Counter-clockwise</button>
    <button id="button-clockwise" class="btn btn-info">Turn Clockwise</button>
  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://192.168.223.93/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn(direction) {
      if (direction == 2) {
        $("#motor-sign").data("motorState", "CWturn");
      } else if (direction == 3) {
        $("#motor-sign").data("motorState", "CCWturn");
      } else {
        $("#motor-sign").data("motorState", "noAction");
      }

      const buttonState = $("#motor-sign").data("motorState");
      const rangeValue = $('#rangeValue').text(); // Get the value of #rangeValue span
      const speedValue = $('#speedValue').text();

      console.log("sent: " + buttonState);
      console.log("sent: " + rangeValue);
      console.log("sent: " + speedValue);

      $.ajax({
        url: "http://192.168.223.93/motor",
        data: {
          state: buttonState,
          stepValue: rangeValue, // Include the range value in the AJAX request
          accelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
          if (responseJSON.motorState == "isTurningCW") {
            $("#motor-sign").css("background", "cyan");
          } else if (responseJSON.motorState == "isTurningCCW") {
            $("#motor-sign").css("background", "orange");
          } else {
            $("#motor-sign").css("background", "gray");
          }
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      setInterval(checkAPIStatus, 2000);

      $("#button-clockwise").click(function () {
        triggerTurn(2);
      });

      $("#button-counter-clockwise").click(function () {
        triggerTurn(3);
      });

      $('#rangeInput').on('input', function() {
    var value = $(this).val();
    $('#rangeValue').text(value);
      });

      $('#speedInput').on('input', function() {
    var value = $(this).val();
    $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}

{{<video src="output8.mp4">}}Sliders control angle & speed of movement{{</video>}}

{{< color-title-enzo text="PCB DESIGN FOR MULTIPLE STEPPERS" >}}

After achieving a satisfactory amount of control over a single motor, I moved on to controlling multiple motors. I my final design, I aim to use threee motors; two for the neck and one for the hip.

I tested the circuit on the breadbord. Each motor has it's own TMC driver, which are connected to the same power & logic source.

{{<image src="i8.jpg" alt="rat nest" size="48%" >}}
{{<image src="i9.jpg" alt="three motors" size="48%" >}}

{{<video src="output9.mp4">}}Three motors turning at the same time{{</video>}}

After making sure the breadboard prototype worked, I started circuit board design using KiCAD.

{{<image-center src="i11.1.jpg" alt="circuit schematic" size="68%" >}}

{{<image-center src="i11.2.jpg" alt="circuit schematic" size="68%" >}}

I milled the board using a Roland SRM-20 milling machine. For more information on the milling process, you can visit [electronics production week](https://burakturkoz.gitlab.io/digital-fabrication-by-burak/assignments/electronics-production/)

{{<video src="output11.1.mp4">}}PCB milling{{</video>}}

{{<image src="i12.jpg" alt="three motor circuit" size="48%" >}}
{{<image src="i13.jpg" alt="three motor circuit" size="48%" >}}

I powered the circuit by a 12V power source that gives max. 1.3A. The motors can all be turned at the same time, or they can be controlled individually.

{{< detail-tag "CODE: Three Motor Smooth Control" >}}

```go

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
int STEP_PIN_C = D9;
int DIR_PIN_C = D8;

# define STEPS 400

void setup() {
  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
  pinMode(STEP_PIN_C,OUTPUT);
  pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {
  digitalWrite(DIR_PIN_C, HIGH);
  digitalWrite(DIR_PIN_A, LOW);
  digitalWrite(DIR_PIN_B, LOW);

  constantAccel(STEP_PIN_A);
  constantAccel(STEP_PIN_B);
  constantAccel(STEP_PIN_C);

}


void constantAccel(int chosenPin){
  int delays[STEPS];
  float angle = 1;
  float accel = 0.01;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< STEPS; i++){
    float d = c0;
    if (i>0)
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    if (d<highSpeed)
      d = highSpeed;
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<STEPS; i++){
    digitalWrite(chosenPin, HIGH);
    delayMicroseconds (delays[i]);
    digitalWrite(chosenPin, LOW);
  }    

    for (int i= 0; i<STEPS; i++){
    digitalWrite(chosenPin, HIGH);
    delayMicroseconds (delays[STEPS-i-1]);
    digitalWrite(chosenPin, LOW);
  }    
}

```
{{< /detail-tag >}}

{{<video src="output11.mp4">}}Three motors with circuit board - individual control{{</video>}}

## SECOND ITERATION

{{< color-title-enzo text="UNIVERSAL JOINTS AND NECK MOVEMENT" >}}

It was time to move past the cardboard prototyping. By using the info I gathered in the lo-fi prototype stage, I modeled a new body and head for the toucan that would be laser cut out of plywood. This time, I used screw-reinforced joints to attach the universal joint. I had learned this method in [laser cutting week](/content/assignments/laser-cutting/) in the very beginning.

{{<image-center src="i16.jpg" alt="laser cut toucan" size="48%" >}}

For the neck, I designed and 3D printed my own universal joint. The first universal joint I modeled had a few problems:

{{<image src="i7.jpg" alt="uj v1" size="48%" >}}
{{<image src="i5.jpg" alt="neck joint" size="48%" >}}
{{<image-center src="s2.jpg" alt="universal joint model" size="68%" >}}

1. The long screws in the middle always got caught in something, and prevented it to turn as intented
2. It was too long, it made the neck of the bird unnaturally long
3. It was too wide

To solve these, I wanted to make it more compact. I found a smaller bearing that I could use, and designed the joint around it.

{{<image-center src="i10.jpg" alt="uj v2" size="48%" >}}

However, this iteration could not turn at all. The cylinder was too tight and the screw I was using could not turn freely. In addition, the smaller screw holes in the middle part were too weak. I experimented with different lenghts, widths and bearing placements. I designed and printed a few more iterations, but all ended with dissappointment.

{{<image-center src="i11.jpg" alt="uj v3" size="48%" >}}

At the end, I decided to go back to the original bearing I was using, and make the joint shorter. I realized that the width was not the problem at all, the length was. I also used smaller screws for the middle part, so that part would be tougher.

Here is a comparison between the first iteration (right) and the last (left).

{{<image-center src="i15.jpg" alt="comparison" size="48%" >}}

Here is the finished universal joint, and how it functions together with the laser-cut plywood body.

{{<image src="i14.jpg" alt="uj v4" size="48%" >}}
{{<image src="i17.jpg" alt="uj v4" size="48%" >}}

{{<video src="output12.mp4">}}Universal joint working{{</video>}}

I also modeled and laser cut some legs for it to stand on. Later on, I will attach the hip motor to them to allow hip movement. I photographed the toucan in it's natural habitat. Special thanks to Saskia for the genius idea.

{{<image src="i19.jpg" alt="second iteration" size="48%" >}}
{{<image src="i20.jpg" alt="second iteration" size="48%" >}}

{{< color-title-enzo text="RE-DESIGNING NETWORK INTERFACE" >}}

After I got the main assembly complete for my second iteration, I started working on the network interface. My aim was to be able to control the speed, steps and turning direction of three stepper motors with a wireless network interface. 

I built this interface on my previous codes from the [networking](/content/assignments/networking/) and [interface and application programming weeks](/content/assignments/interface-and-application-programming/).

You can see the full process of trial-and-error documented here in [the Code Dump](/content/code-dump/). Here, I will try to summarize my progress.

As I built on the interfaces from the previous weeks, I updated the type of information that gets transferred between the interface and XIAO. I made the interface send seperate strings of data for "direction", "speed" and "steps" for each motor. I started with one motor, then increased it to two. I was originally going to add a third motor as well, but did not have time. However, it can be added easily by replicating the code if one wishes to do so.

I made a graphical user interface for the information to be clearer. In the interface below, the user can select the angle and direction for each motor, while the speed remains the same for both.

{{<image src="s7.jpg" alt="GUI" size="80%" >}}

For the Arduino side, I experimented with both accelerating and non-accelerating setups for the steppers. They too, can be found in [the Code Dump](/content/code-dump/). Here are two examples: one of them uses keyboard controls to control a single motor, and the second one utilizes the GUI to it's full extent by controlling two motors with acceleration. Although the second one has it's issues, it is the one used in the examples below until the final code comes into play.

{{< detail-tag "CODE: Precise Stepper Motor Control / WiFi/ Keyboard Control [WORKING v1]" >}}

#### Arduino IDE
```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char *ssid = "Fablab";
const char *password = "Fabricationlab1";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
int STEP_PIN_C = D9;
int DIR_PIN_C = D8;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
  pinMode(STEP_PIN_C,OUTPUT);
  pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
  
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int state;  // motor state
    int stepValue; // range slider value, which will be turned into stepCount
    float accelValue; // range slider value, which will be turned into accel

    if (request->hasParam("state")) {
      // The incoming params are Strings
      String param = request->getParam("state")->value();
      // .. so we have to interpret or cast them
      if (param == "CWturn") {
        state = 2;
      } else if (param == "CCWturn") {
        state = 3;
      } else {
        state = 0;
      }
    } else {
      state = 0;
    }

    if (request->hasParam("stepValue")){
      stepValue = request->getParam("stepValue")->value().toInt();
    }

    if (request->hasParam("accelValue")){
      accelValue = request->getParam("accelValue")->value().toFloat();
    }

     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement

    if (state == 2) {
      Serial.println("turningcw");
      digitalWrite(DIR_PIN_A, LOW);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCW";
    } else if (state == 3) {
      Serial.println("turningCCW");
      digitalWrite(DIR_PIN_A, HIGH);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCCW";
    } else {
      stateString = "notTurning";
    }

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}

void constantAccel(float accelVal){
  int delays[stepCount];
  float angle = 1;
  float accel = accelVal;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< stepCount; i++){
    float d = c0;
    if (i>0){
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    }      
    if (d<highSpeed){      
      d = highSpeed;
    }
      
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN_A, HIGH);
    delayMicroseconds (delays[i]);
    digitalWrite(STEP_PIN_A, LOW);
  }    

    for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN_A, HIGH);
    delayMicroseconds (delays[stepCount-i-1]);
    digitalWrite(STEP_PIN_A, LOW);
  }    
}

void loop() {

}

```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>BootStrap Rest DEMO</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>stepper control test</h1>
    <div class="form-group">
      <label for="rangeInput">Angle Input:</label>
      <input type="range" class="form-range" id="rangeInput" min="0" max="192" value="50"> 
    </div>
    <p>Selected value: <span id="rangeValue"> 50 </span></p>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="0.0001" max="0.03"  step="0.0001" value="0.01"> 
    </div>
    <p>Selected value: <span id="speedValue"> 0.01 </span></p>
    
    <div id="motor-sign">Motor Rotation</div>
    <button id="button-counter-clockwise" class="btn btn-warning">Turn Counter-clockwise</button>
    <button id="button-clockwise" class="btn btn-info">Turn Clockwise</button>
  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn(direction) {
      if (direction == 2) {
        $("#motor-sign").data("motorState", "CWturn");
      } else if (direction == 3) {
        $("#motor-sign").data("motorState", "CCWturn");
      } else {
        $("#motor-sign").data("motorState", "noAction");
      }

      const buttonState = $("#motor-sign").data("motorState");
      const rangeValue = $('#rangeValue').text(); // Get the value of #rangeValue span
      const speedValue = $('#speedValue').text();

      console.log("sent: " + buttonState);
      console.log("sent: " + rangeValue);
      console.log("sent: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          state: buttonState,
          stepValue: rangeValue, // Include the range value in the AJAX request
          accelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
          if (responseJSON.motorState == "isTurningCW") {
            $("#motor-sign").css("background", "cyan");
          } else if (responseJSON.motorState == "isTurningCCW") {
            $("#motor-sign").css("background", "orange");
          } else {
            $("#motor-sign").css("background", "gray");
          }
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

        // Keyboard event listener
  $(document).on('keypress', function (event) {
    var keyPressed = event.key;
    if (keyPressed === 'd' || keyPressed === 'D') {
      triggerTurn(2); // Simulate Turn Clockwise button click
    } else if (keyPressed === 'a' || keyPressed === 'A') {
      triggerTurn(3); // Simulate Turn Counter-clockwise button click
    }
      });

      setInterval(checkAPIStatus, 2000);

      $("#button-clockwise").click(function () {
        triggerTurn(2);
      });

      $("#button-counter-clockwise").click(function () {
        triggerTurn(3);
      });

      $('#rangeInput').on('input', function() {
    var value = $(this).val();
    $('#rangeValue').text(value);
      });

      $('#speedInput').on('input', function() {
    var value = $(this).val();
    $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```
{{< /detail-tag >}}

{{< detail-tag "CODE: Precise Stepper Motor Control / WiFi/ Interface With Two Motors [PARTIALLY WORKING v3]" >}}

### Precise Stepper Motor Control / WiFi/ Interface With Two Motors [PARTIALLY WORKING v3]

#### Arduino IDE

- There is acceleration going on, but the motors start and stop two times. May work on it more.

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char *ssid = "Fablab";
const char *password = "Fabricationlab1";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
int STEP_PIN_C = D9;
int DIR_PIN_C = D8;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
  pinMode(STEP_PIN_C,OUTPUT);
  pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
  
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int stepValueA;
    int stepValueB;
    float accelValue; 

    if (request->hasParam("sentStepValueA")){
      stepValueA = request->getParam("sentStepValueA")->value().toInt();
    }

    if (request->hasParam("sentStepValueB")){
      stepValueB = request->getParam("sentStepValueB")->value().toInt();
    }

    if (request->hasParam("sentAccelValue")){
      accelValue = request->getParam("sentAccelValue")->value().toFloat();
    }


    if (request->hasParam("sentDirA")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirA")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_A, LOW);
        Serial.println("dir a ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_A, HIGH);
           Serial.println("dir a cw");
      } else {
        stepValueA = 0;
      }
    } 

    if (request->hasParam("sentDirB")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirB")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_B, LOW);
        Serial.println("dir b ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_B, HIGH);
        Serial.println("dir b cw");
      } else {
        stepValueB=0;
      }
    }     
     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement
    stateString = "done";
        
    Serial.println(stepValueA); 
    Serial.println(stepValueB);    
    Serial.println(accelValue);   

    motorMove(stepValueA, stepValueB, accelValue);

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}


void motorMove(int stepValueA, int stepValueB, float accelValue){
  int delaysA[stepValueA];
  float angleA = 1;
  float accelA = accelValue;
  float c0A = 2000 * sqrt(2 * angleA / accelA ) * 0.67703;
  float lastDelayA = 0;
  int highSpeedA = 100;
  for (int i=0; i< stepValueA; i++){
    float d = c0A;
    if (i>0){
      d = lastDelayA - (2 * lastDelayA)/(4*i+1);
    }      
    if (d<highSpeedA){      
      d = highSpeedA;
    }
      
    delaysA[i] = d;
    lastDelayA = d;   
  }


  int delaysB[stepValueB];
  float angleB = 1;
  float accelB = accelValue;
  float c0B = 2000 * sqrt(2 * angleB / accelB ) * 0.67703;
  float lastDelayB = 0;
  int highSpeedB = 100;
  for (int i=0; i< stepValueB; i++){
    float d = c0B;
    if (i>0){
      d = lastDelayB - (2 * lastDelayB)/(4*i+1);
    }      
    if (d<highSpeedB){      
      d = highSpeedB;
    }
      
    delaysB[i] = d;
    lastDelayB = d;   
  }

  int i = 0;
  int j = 0;

  while (i < stepValueA || j < stepValueB) {
    if (i < stepValueA) {
      digitalWrite(STEP_PIN_A, HIGH);
      delayMicroseconds(delaysA[i]);
      digitalWrite(STEP_PIN_A, LOW);
      i++;
    }

    if (j < stepValueB) {
      digitalWrite(STEP_PIN_B, HIGH);
      delayMicroseconds(delaysB[j]);
      digitalWrite(STEP_PIN_B, LOW);
      j++;
    }
  }

  i = 0;
  j = 0;

  while (i < stepValueA || j < stepValueB) {
    if (i < stepValueA) {
      digitalWrite(STEP_PIN_A, HIGH);
      delayMicroseconds(delaysA[stepValueA-i-1]);
      digitalWrite(STEP_PIN_A, LOW);
      i++;
    }

    if (j < stepValueB) {
      digitalWrite(STEP_PIN_B, HIGH);
      delayMicroseconds(delaysB[stepValueB-j-1]);
      digitalWrite(STEP_PIN_B, LOW);
      j++;
    }
  }
  

}

void loop() {

}
```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Toucan Motor Control Interface</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>Toucan Motor Control Interface</h1>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="0.0001" max="0.03" step="0.0001" value="0.01">
    </div>
    <p>Selected value: <span id="speedValue"> 0.01 </span></p>

    <h2>Motor A</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input A:</label>
      <input type="range" class="form-range" id="stepInputA" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-A"> 50 </span></p>
    
    <button id="button-motor-A-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-A-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-A-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor A Direction: <span id="direction-motor-A"> none </span></p>

    <h2>Motor B</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input B:</label>
      <input type="range" class="form-range" id="stepInputB" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-B"> 50 </span></p>

    <button id="button-motor-B-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-B-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-B-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor B Direction: <span id="direction-motor-B"> none </span></p>

    <br>
    <button id="button-turn" type="button" class="btn btn-primary btn-lg">TURN!</button>


  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn() {
      const directionMotorA = $("#direction-motor-A").text();
      const directionMotorB = $("#direction-motor-B").text();
      const stepValueA = $('#step-value-A').text();
      const stepValueB = $('#step-value-B').text();
      const speedValue = $('#speedValue').text();

      console.log("DirA: " + directionMotorA);
      console.log("DirB: " + directionMotorB);
      console.log("StepA: " + stepValueA);
      console.log("StepB: " + stepValueB);
      console.log("Speed: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          sentDirA: directionMotorA,
          sentDirB: directionMotorB,
          sentStepValueA: stepValueA,
          sentStepValueB: stepValueB,
          sentAccelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      // Keyboard event listener
      $(document).on('keypress', function (event) {
        var keyPressed = event.key;
        if (keyPressed === 'd' || keyPressed === 'D') {
          triggerTurn(2); // Simulate Turn Clockwise button click
        } else if (keyPressed === 'a' || keyPressed === 'A') {
          triggerTurn(3); // Simulate Turn Counter-clockwise button click
        }
      });

      setInterval(checkAPIStatus, 2000);

      // Button click event
  // Motor A buttons
  $("#button-motor-A-left, #button-motor-A-none, #button-motor-A-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-A").text(direction);
  });

  // Motor B buttons
  $("#button-motor-B-left, #button-motor-B-none, #button-motor-B-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-B").text(direction);
  });

      $("#button-turn").click(function () {
        triggerTurn();
      });

      $('#stepInputA').on('input', function () {
        var value = $(this).val();
        $('#step-value-A').text(value);
      });

      $('#stepInputB').on('input', function () {
        var value = $(this).val();
        $('#step-value-B').text(value);
      });

      $('#speedInput').on('input', function () {
        var value = $(this).val();
        $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}

{{< color-title-enzo text="HEAD MOVEMENT TESTING" >}}

After creating the network interface code for the steppers, I started to try it out on moving the toucan's head. As I started, I saw the first problem: the plywood beak was too heavy for the motor to move. It often fell down to one side, because it was so front-heavy.

Here is a video of me barely moving the beak with the steppers:

{{<video src="output13.mp4">}}Toucan beaks are very heavy{{</video>}}

The solution I found at this stage was to cut off the beak. I found it to be much easier to turn the head after that. It could do both the slower and rapid movements clearly.

{{<video src="output16.mp4">}}Toucan beaks are very heavy{{</video>}}

But another issue arose after this. Because of the way the wires were angled and put through the holes in the head, they were getting stuck all the time. You can see below how I constantly have to adjust the wire-head connections to get them unstuck. I experimented for a while with different connection styles and hole diameters for the wire-head connections.

{{<video src="output14.mp4">}}Toucan beaks are very heavy{{</video>}}

{{<image src="i21.jpg" alt="wire experiments" size="48%" >}}
{{<image src="i22.jpg" alt="wire experiments" size="48%" >}}

The solution I came up with to this problem was to modify the head shape to make the top connection hole horizontal. In the previous iterations, the side-to-side movements were achieved by a wire connected to a horizontally drilled hole; while the top-down movement wire was attached to a vertical hole. This was making the wire get stuck due to the turning angles.

Here is the new and improved head shape:

{{<image src="i25.jpg" alt="new head shape" size="48%" >}}
{{<image src="i26.jpg" alt="new head shape" size="48%" >}}

{{<image src="i27.jpg" alt="new head shape" size="48%" >}}
{{<image src="i23.jpg" alt="new head shape" size="48%" >}}

With this new shape, I was able to achieve much smoother movements without the wires getting stuck in the holes. With that out of the way, I moved on to my third iteration.

## THIRD ITERATION

{{< color-title-enzo text="ACRYLIC BODY AND ELECTRONICS BOX" >}}

I decided that the final form of the toucan would be made out of a semi-transparent 3mm acrylic body. That would give it an overall cleaner look. I modified my Fusion file for the necessary adjustments regarding the nut & screw connections, decided on the tolerances & kerf, and started cutting.

{{<image src="i28.jpg" alt="new body" size="48%" >}}

After I cut it, I moved on to assembling it with my usual "screw-reinforced joint" method, trying to get a tight fit with the nuts. However, I quickly learned that acryclic is much more brittle than plywood when you force something on it.

{{<image src="i29.jpg" alt="new body" size="48%" >}}

I made structure stronger, loosened the tolerances, and re-cut. I used super-glue in the parts that turned out too loose. It is better to glue it than risk breakage.

{{<image src="i30.jpg" alt="third iteration body" size="48%" >}}
{{<image src="i32.jpg" alt="third iteration body" size="48%" >}}

{{<image src="i31.jpg" alt="third iteration body" size="48%" >}}
{{<image src="i33.jpg" alt="third iteration body" size="48%" >}}  

The design also needed a place to put my electronic components in. I laser cut a box with dividers and outlets, that would also act as a stand for the toucan.

In the box, I put my circuit board, 12V adapter and the cabling in-between. The toucan stands on the box with a nuts-and-bolt connection.

{{<image src="i34.jpg" alt="component box" size="48%" >}}
{{<image src="i35.jpg" alt="component box" size="48%" >}}  

{{<image src="i36.jpg" alt="component box" size="48%" >}}
{{<image src="i37.jpg" alt="component box" size="48%" >}}  

{{< color-title-enzo text="VACUUM FORMING THE HEAD" >}}

Previously I mentioned that the laser-cut beak was too heavy to turn. However, a toucan is not a toucan without a beak.

So the solution I found to make a three dimensional beak while keeping it light, was vacuum forming. I had used the [wildcard week](/content/assignments/wildcard-week/) to make a proof-of-concept for this design and it worked. So now, I scaled up the design to use in the final product.

I overlayed the design on top of my laser cut files in Fusion, to make sure that it would fit over the laser-cut assembly. This allows me to spot errors before I make a mistake that costs me milling & vacuum forming time (which is a lot of time).

{{<image src="s4.jpg" alt="overlaying" size="48%" >}}
{{<image src="s6.jpg" alt="Fusion CAM" size="48%" >}}

Again, I used Fusion's CAM interface to plan out the milling paths. In addition to the workflow in the [wildcard week](/content/assignments/wildcard-week/), I also put a "Face Milling" step first to make sure that the larger surface I was working with was level. I then milled the vacuum forming mold out of sikablock.

{{<video src="output17.mp4">}}Milling sikablock{{</video>}}

{{<image-center src="i38.jpg" alt="vacuum form mold" size="48%" >}}

I vacuum formed the head of the same 0.5mm acryclic sheet, and placed it on the top of the head. Here is a few photos of how the toucan looked at this stage, when assembled fully.

{{<image src="i39.jpg" alt="final form of the toucan" size="48%" >}}
{{<image src="i40.jpg" alt="final form of the toucan" size="48%" >}}

{{<image src="i41.jpg" alt="final form of the toucan" size="48%" >}}
{{<image src="i43.jpg" alt="final form of the toucan" size="48%" >}}

{{< color-title-enzo text="REFINING HEAD MOVEMENTS AND CODE" >}}

As I went on with these stages, I was always testing head movements on the side. After the vacuum forming & assembly, I continued my tests.

The issue now -which had shown itself when I moved on to the acrylic body & new head shape- was the steppers not generating enough torque. In the first video down below, you can see how the head snaps back after it reaches a certain angle and the stepper tries to turn it further. This was an issue with the stepper's capabilities, since I was able to turn the head at the same angle when I manually turn the stepper with my hands. Rather than the wire connections or angles being a problem, this time the stepper was giving me trouble.

{{<video src="output18.mp4">}}stepper snapping back after reaching a certain angle{{</video>}}

{{<video src="output19.mp4">}}I can turn the head to that angle with manual hand turn{{</video>}}

I tried altering the code, disconnecting the third stepper entirely and some more additional fixes, but none of them worked to the extent I wanted. Tinkering with the code was my best option in this stage. The previous code used an acceleration setup for the steppers, meaning the turning motion started slow, accelerated, and slowly stopped. I wanted it to be this way to avoid very snappy unnatural movements. However, this code was starting to act strange when two steppers were involved.

Previously, I had tried to make it work by introducing a while loop that ran independently for two steppers within itself. However, this caused the steppers to act strange when the two of them had different step counts, and disrupted the whole motion. The code snippet is down below.


{{< detail-tag "CODE: Motor Move Function with Independently Accelerating Motors [PARTIALLY WORKING]" >}}


```go

void motorMove(int stepValueA, int stepValueB, float accelValue){
  int delaysA[stepValueA];
  float angleA = 1;
  float accelA = accelValue;
  float c0A = 2000 * sqrt(2 * angleA / accelA ) * 0.67703;
  float lastDelayA = 0;
  int highSpeedA = 100;
  for (int i=0; i< stepValueA; i++){
    float d = c0A;
    if (i>0){
      d = lastDelayA - (2 * lastDelayA)/(4*i+1);
    }      
    if (d<highSpeedA){      
      d = highSpeedA;
    }
      
    delaysA[i] = d;
    lastDelayA = d;   
  }


  int delaysB[stepValueB];
  float angleB = 1;
  float accelB = accelValue;
  float c0B = 2000 * sqrt(2 * angleB / accelB ) * 0.67703;
  float lastDelayB = 0;
  int highSpeedB = 100;
  for (int i=0; i< stepValueB; i++){
    float d = c0B;
    if (i>0){
      d = lastDelayB - (2 * lastDelayB)/(4*i+1);
    }      
    if (d<highSpeedB){      
      d = highSpeedB;
    }
      
    delaysB[i] = d;
    lastDelayB = d;   
  }

  int i = 0;
  int j = 0;

  while (i < stepValueA || j < stepValueB) {
    if (i < stepValueA) {
      digitalWrite(STEP_PIN_A, HIGH);
      delayMicroseconds(delaysA[i]);
      digitalWrite(STEP_PIN_A, LOW);
      i++;
    }

    if (j < stepValueB) {
      digitalWrite(STEP_PIN_B, HIGH);
      delayMicroseconds(delaysB[j]);
      digitalWrite(STEP_PIN_B, LOW);
      j++;
    }
  }

  i = 0;
  j = 0;

  while (i < stepValueA || j < stepValueB) {
    if (i < stepValueA) {
      digitalWrite(STEP_PIN_A, HIGH);
      delayMicroseconds(delaysA[stepValueA-i-1]);
      digitalWrite(STEP_PIN_A, LOW);
      i++;
    }

    if (j < stepValueB) {
      digitalWrite(STEP_PIN_B, HIGH);
      delayMicroseconds(delaysB[stepValueB-j-1]);
      digitalWrite(STEP_PIN_B, LOW);
      j++;
    }
  }

}

```

{{< /detail-tag >}}

After being unable to solve this code related problem, I turned to simplifying the program. I got rid of acceleration, and simultaneous movements. I programmed the motors to move sequentially, and without any acceleration. This certainly made things easier when trying to figure out the head movements. Here is the final code, including the jQuery and Arduino sides, that I used in the final video:

{{< detail-tag "CODE: Sequential Stepper Motor Control / WiFi/ Interface With Two Motors [WORKING - VIDEO 07.06.23]" >}}

#### Arduino IDE

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char *ssid = "Fablab";
const char *password = "Fabricationlab1";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
//int STEP_PIN_C = D9;
//int DIR_PIN_C = D8;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
 // pinMode(STEP_PIN_C,OUTPUT);
 // pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
  
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int stepValueA;
    int stepValueB;
    int accelValue; 

    if (request->hasParam("sentStepValueA")){
      stepValueA = request->getParam("sentStepValueA")->value().toInt();
    }

    if (request->hasParam("sentStepValueB")){
      stepValueB = request->getParam("sentStepValueB")->value().toInt();
    }

    if (request->hasParam("sentAccelValue")){
      accelValue = request->getParam("sentAccelValue")->value().toInt();
    }


    if (request->hasParam("sentDirA")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirA")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_A, HIGH);
        Serial.println("dir a ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_A, LOW);
           Serial.println("dir a cw");
      } else {
        stepValueA = 0;
      }
    } 

    if (request->hasParam("sentDirB")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirB")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_B, HIGH);
        Serial.println("dir b ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_B, LOW);
        Serial.println("dir b cw");
      } else {
        stepValueB=0;
      }
    }     
     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement
    stateString = "done";
        
    Serial.println(stepValueA); 
    Serial.println(stepValueB);    
    Serial.println(accelValue);   

    motorMove(stepValueA, stepValueB, accelValue);
    

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}


void motorMove(int stepValueA, int stepValueB, float accelValue) {
  int i = 0;
  int j = 0;

  while (i < stepValueA) {
    digitalWrite(STEP_PIN_A, HIGH);
    delayMicroseconds(accelValue);
    digitalWrite(STEP_PIN_A, LOW);
    i++;
  }

  while (j < stepValueB) {
    digitalWrite(STEP_PIN_B, HIGH);
    delayMicroseconds(accelValue);
    digitalWrite(STEP_PIN_B, LOW);
    j++;
  }
}



void loop() {

}
```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Toucan Motor Control Interface</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>Toucan Motor Control Interface</h1>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="1000" max="30000" value="5000">
    </div>
    <p>Selected value: <span id="speedValue"> 5000 </span></p>

    <h2>Motor A</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input A:</label>
      <input type="range" class="form-range" id="stepInputA" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-A"> 50 </span></p>
    
    <button id="button-motor-A-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-A-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-A-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor A Direction: <span id="direction-motor-A"> none </span></p>

    <h2>Motor B</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input B:</label>
      <input type="range" class="form-range" id="stepInputB" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-B"> 50 </span></p>

    <button id="button-motor-B-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-B-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-B-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor B Direction: <span id="direction-motor-B"> none </span></p>

    <br>
    <button id="button-turn" type="button" class="btn btn-primary btn-lg">TURN!</button>


  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn() {
      const directionMotorA = $("#direction-motor-A").text();
      const directionMotorB = $("#direction-motor-B").text();
      const stepValueA = $('#step-value-A').text();
      const stepValueB = $('#step-value-B').text();
      const speedValue = $('#speedValue').text();

      console.log("DirA: " + directionMotorA);
      console.log("DirB: " + directionMotorB);
      console.log("StepA: " + stepValueA);
      console.log("StepB: " + stepValueB);
      console.log("Speed: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          sentDirA: directionMotorA,
          sentDirB: directionMotorB,
          sentStepValueA: stepValueA,
          sentStepValueB: stepValueB,
          sentAccelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      // Keyboard event listener
      $(document).on('keypress', function (event) {
        var keyPressed = event.key;
        if (keyPressed === 'd' || keyPressed === 'D') {
          triggerTurn(2); // Simulate Turn Clockwise button click
        } else if (keyPressed === 'a' || keyPressed === 'A') {
          triggerTurn(3); // Simulate Turn Counter-clockwise button click
        }
      });

      setInterval(checkAPIStatus, 2000);

      // Button click event
  // Motor A buttons
  $("#button-motor-A-left, #button-motor-A-none, #button-motor-A-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-A").text(direction);
  });

  // Motor B buttons
  $("#button-motor-B-left, #button-motor-B-none, #button-motor-B-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-B").text(direction);
  });

      $("#button-turn").click(function () {
        triggerTurn();
      });

      $('#stepInputA').on('input', function () {
        var value = $(this).val();
        $('#step-value-A').text(value);
      });

      $('#stepInputB').on('input', function () {
        var value = $(this).val();
        $('#step-value-B').text(value);
      });

      $('#speedInput').on('input', function () {
        var value = $(this).val();
        $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}

{{<video src="output20.mp4">}}Sequential movement{{</video>}}

## FINAL PRODUCT

{{<video src="final2.mp4">}}Final project presentation video{{</video>}}

{{<image src="i47.jpg" alt="final gallery" size="100%" >}}
{{<image src="i48.jpg" alt="final gallery" size="100%" >}}

{{<image src="i51.jpg" alt="final gallery" size="100%" >}}
{{<image src="i50.jpg" alt="final gallery" size="100%" >}}

{{<image src="i52.jpg" alt="final gallery" size="100%" >}}
{{<image src="i53.jpg" alt="final gallery" size="100%" >}}

{{<image src="i54.jpg" alt="final gallery" size="100%" >}}
{{<image src="i55.jpg" alt="final gallery" size="100%" >}}

{{< color-title-enzo text="FURTHER IMPROVEMENTS" >}}

I am currently improving Enzo's electronics integration solutions, as well as updating it's stepper motors.

Below is a model of the new electronics box. The inner part will be 3D printed, while the outer section will be CNC milled out of sikablock.

{{<image src="news3.jpg" alt="improve" size="49%" >}}
{{<image src="news.jpg" alt="improve" size="49%" >}}


