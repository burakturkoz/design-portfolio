+++
title= "Contact"
+++

{{<image-center src="toucancontact.png" alt="contact toucan" size="20%" >}}

{{< project-type-subtitle text="burak_turkoz@outlook.com" >}}
{{< project-type-subtitle text="burak.turkoz@aalto.fi (secondary)" >}}
{{< project-type-subtitle link="https://www.linkedin.com/in/burak-turkoz/" text="LinkedIn" >}}
