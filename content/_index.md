+++
title = "Works"
+++

<h2 class = "intro-title">Hi! I'm Burak Türköz, <br>industrial designer and maker.</h2>

<h3 class = "intro-subtitle">I am a Helsinki-based industrial designer and maker, specializing in rapid prototyping.</h3>

<h3 class = "intro-subtitle">I am open to freelance work in product design, prototyping or maker education. You can reach out to me at <a href="mailto:burak_turkoz@outlook.com">burak_turkoz@outlook.com</a> or contact me through <a href = "https://www.linkedin.com/in/burak-turkoz/" target="_blank">LinkedIn.</a></h3>

{{< intro-section-title text="SELECTED PROJECTS" >}}

{{< box link="mobile-lab" no="box-mobile-lab" title="MAKING A MOBILE MAKERSPACE" subtitle="Master's Thesis Project" image="toucan-mobile-lab.png" hoverImage="mobile-lab-hover.jpg" alt="mobile-lab-toucan" >}}
Makerspace Design, Maker Education
{{< /box >}}

{{< box link="enzo" no="box-enzo" title="ENZO THE ANIMATRONIC TOUCAN" subtitle="Making a Robotic Bird" image="toucan-enzo.png" hoverImage="enzo-hover.jpg" alt="enzo-toucan" >}}
Documentation Page, Digital Fabrication / 2023 
{{< /box >}}

{{< box link="psp-v" no="box-psp" title="PSP: V" subtitle="Next-Generation PlayStation Portable" image="toucan-psp.png" hoverImage="psp-hover.jpg" alt="psp-toucan" >}}
Industrial Design / 2021
{{< /box >}}

{{< box link="inflatable-machine" no="box-inflatable-machine" title="INFLATABLE MAKING MACHINE" subtitle="A CNC Welder That Can Make Baloons" image="toucan-inflatable.png" hoverImage="inflatable-hover.jpg" alt="inflatable-toucan" >}}
Documentation Page, Machine Design, Digital Fabrication / 2024 
{{< /box >}}

{{< box link="orbit" no="box-orbit" title="ORBIT" subtitle="Tabletop Gaming for 2032" image="toucan-orbit.png" hoverImage="orbit-hover.jpg" alt="orbit toucan" >}}
Industrial Design, Speculative Design / 2022
{{< /box >}}

{{< box link="cabinet-of-curiosities" no="box-cabinet" title="A CABINET OF CURIOSITIES" subtitle="Interactive Display Cabinet for a Makerspace" image="toucan-cabinet.png" hoverImage="cabinet-hover.jpg" alt="cabinet-toucan" >}}
Documentation Page, Digital Fabrication / 2023
{{< /box >}}

{{< box link="sands-of-time" no="box-sands" title="SANDS OF TIME" subtitle="Exhibition Design for the Rosetta Stone" image="toucan-sands.png" hoverImage="sands-hover.jpg" alt="sans of time toucan" >}}
Exhibition Design / 2022
{{< /box >}}

{{< box link="dry-noon" no="box-drynoon" title="DRY NOON" subtitle="A Game of Quickdraw With Arduinos... and Hairdryers" image="toucan-dry.png" hoverImage="dry-hover.jpg" alt="dry noon toucan" >}}
Documentation Page, Physical Computing / 2022
{{< /box >}}

{{< box link="https://burakturkoz.gitlab.io/how-i-learned-to-stop-worrying-and-love-the-docs/" no="box-game" title="HOW I LEARNED TO STOP WORRYING & LOVE THE DOCS" subtitle="A Frustrating Text-Based Adventure" image="toucan-docs.png" hoverImage="docs-hover.jpg" alt="docs-toucan" >}}
Creative Coding / 2022
{{< /box >}}

{{< box link="toz" no="box-toz" title="TÖZ" subtitle="Shamanic Prop Design" image="toucan-toz.png" hoverImage="toz-hover.jpg" alt="toz toucan" >}}
Prop Design, Concept Art / 2021
{{< /box >}}

{{< box link="matchbox" no="box-matchbox" title="MATCHBOX" subtitle="Lighting & Power Unit" image="toucan-ignite.png" hoverImage="matchbox-hover.jpg" alt="matchbox toucan" >}}
Industrial Design / 2021
{{< /box >}}
