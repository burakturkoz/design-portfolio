+++
title="A Cabinet of Curiosities"
+++

{{< project-subtitle text="Interactive Display Cabinet for a Fablab" >}}

{{< project-type-subtitle text="// Digital Fabrication //" >}}

{{<image src="cover.jpg" alt="cover photo" size="100%" >}}

## OVERVIEW

In the summer of 2023, I completed a 2-month internship at [Fablab powered by Orange](https://fablabwarszawa.pl/en) at Warsaw. It is a makerspace operated by [Robisz.to](https://robisz.to/), a non-governmental organization for maker activities. The Fablab facilitates maker education for the public by holding open days, project development courses and workshops.

During my internship, I discovered they needed a centerpiece for their lobby space. Something that could draw the attention of visitors, and act as a way to showcase the work that is done here. I used different manufacturing methods available at the workshops a the lab (woodworking, electronics, 3D printing, laser cutting and CNC-milling). The cabinet is powered by an Arduino, and has integrated moving components and LEDs. 

**Project Type:** Individual work

**Location:** Fablab powered by Orange, Warsaw.

**Year:** 2023

## PROTOTYPING

{{< color-title-cabinet text="IDEATION" >}}

I started with sketching ideas. I talked to the educators around the lab to get an idea about what they would like to represent their Fablab. We settled on the idea of displaying the manufacturing methods available in the lab. I decided to design something that would be built by using the different fabrication methods here, and in addition, would display the artifacts made with them.

I made a parametric CAD model of my design, so I could adjust the size of the assembly if needed.

{{<image src="i1.1.jpg" alt="sketches" size="62%" >}}
{{<image src="i2.jpg" alt="cardboard" size="34%" >}}

{{<image-center src="s1.jpg" alt="CAD" size="100%" >}}

{{< color-title-cabinet text="ELECTRONICS AND MOTORS" >}}

I started prototyping the moving parts. An Arduino Uno was used to control a stepper motor thorugh a shield. I laser cut and 3D printed a crank mechanism.

{{<image src="i3.jpg" alt="electronics" size="48%" >}}
{{<image src="i5.jpg" alt="electronics" size="48%" >}}

{{<video src="output1.mp4">}}Crank mechanism{{</video>}}

The other major component was a LED strip, for which I used relays to control.

{{<image-center src="i6.jpg" alt="led strips" size="48%" >}}

{{< color-title-cabinet text="MANUFACTURING" >}}

I used 12mm birch plywood to manufacture the cabinet. The design consists of 3 modular pieces, which can be stacked on top of each other. They can be carried and displayed separately if needed.

{{<video src="output2.mp4">}}CNC milling{{</video>}}

{{<image src="i8.jpg" alt="cnc milling" size="48%" >}}
{{<image src="i9.jpg" alt="cnc milling" size="48%" >}}

The moving components inside are manufactured by laser cutting and 3D printing.

{{<image src="i7.jpg" alt="small components" size="48%" >}}
{{<image src="i10.jpg" alt="small components" size="48%" >}}

{{< color-title-cabinet text="ASSEMBLY" >}}

{{<image src="i11.jpg" alt="assembly" size="48%" >}}
{{<image src="i12.jpg" alt="assembly" size="48%" >}}

{{<image src="i13.jpg" alt="assembly" size="48%" >}}
{{<image src="i14.jpg" alt="assembly" size="48%" >}}

{{<image src="i16.jpg" alt="assembly" size="48%" >}}
{{<image src="i15.jpg" alt="assembly" size="48%" >}}

{{<image src="i17.jpg" alt="assembly" size="48%" >}}
{{<image src="i18.jpg" alt="assembly" size="48%" >}}

The back of the cabinet can be slid open to access the electronics.

{{<image src="i20.jpg" alt="assembly" size="48%" >}}
{{<image src="i19.jpg" alt="assembly" size="48%" >}}

{{<video src="output3.mp4">}}Moving components{{</video>}}

Finally, I added the LED strips along the letters.

{{<image-center src="i22.jpg" alt="assembly" size="48%" >}}

{{< color-title-cabinet text="FINAL PRODUCT" >}}

After assembly was complete, we decorated the cabinet with the objects made in the Fablab.

{{<image-center src="i24.jpg" alt="final" size="99%" >}}

{{<image-center src="i25.jpg" alt="final" size="99%" >}}

{{<image-center src="i23.jpg" alt="final" size="99%" >}}

  <div class="vertical-video-container">
    <video controls>
        <source src="v5.mp4" type="video/mp4">
    </video>
  </div>
  <p class="vertical-video-text">Final video</p>
