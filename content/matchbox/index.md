+++
title="Matchbox"
+++

{{< project-subtitle text="Lighting and Power Unit for Disaster Relief Camps " >}}

{{< project-type-subtitle text="// Product Design //" >}}

## OVERVIEW

In disaster relief camps today, electrical power is provided through diesel generators. These generators are placed far away from the living areas due to their loud noises. To provide electricity and light for the living areas in the camps, meters of extensions cables need to be drawn from these generators. This creates a logistical hardship and a potential safety hazard. Matchbox aims to solve this problem by providing a cordless and silent solution that provides battery-powered electricity and light to the living areas of disaster camps.

**Project Type:** Individual work for competition

**Organization:** IMMIB Industrial Design Competition 2021

**Year:** 2021

## FINAL PRODUCT

{{<image src="match-1.jpg" alt="final product" size="100%" >}}

{{<image src="match-2.jpg" alt="final product" size="100%" >}}

{{<image src="match-3.jpg" alt="final product" size="100%" >}}


